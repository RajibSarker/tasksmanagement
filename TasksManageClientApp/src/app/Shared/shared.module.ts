import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { HomeComponent } from "./home/home.component";
import { NavComponent } from "./nav/nav.component";

@NgModule({
    imports: [
        CommonModule
    ],
    declarations: [
        HomeComponent,
        NavComponent
    ],
    exports:[
        HomeComponent,
        NavComponent
    ]
})
export class SharedModule { }
