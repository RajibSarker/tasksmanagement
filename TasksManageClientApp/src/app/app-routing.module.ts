import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";

const routes: Routes = [
    // {
    //     path: "admin",
    //     loadChildren: () =>
    //         import("./components/admin/admin.module").then((m) => m.AdminModule),
    // },
    
    
    // { path: "page-not-found", component: PageNotFoundComponent },
    // { path: "**", redirectTo: "/page-not-found" },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {
            preloadingStrategy: PreloadAllModules,
            scrollPositionRestoration: "enabled",
            initialNavigation: "enabled",
            onSameUrlNavigation: "reload",
        }),
    ],
    exports: [RouterModule],
})
export class AppRoutingModule { }