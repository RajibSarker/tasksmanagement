﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TasksManagement.Migrations
{
    public partial class updatedinspecitonmodels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Inspections_InspecitonTypes_InspecitonTypeId",
                table: "Inspections");

            migrationBuilder.AlterColumn<long>(
                name: "InspecitonTypeId",
                table: "Inspections",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AddForeignKey(
                name: "FK_Inspections_InspecitonTypes_InspecitonTypeId",
                table: "Inspections",
                column: "InspecitonTypeId",
                principalTable: "InspecitonTypes",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Inspections_InspecitonTypes_InspecitonTypeId",
                table: "Inspections");

            migrationBuilder.AlterColumn<long>(
                name: "InspecitonTypeId",
                table: "Inspections",
                type: "bigint",
                nullable: false,
                defaultValue: 0L,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Inspections_InspecitonTypes_InspecitonTypeId",
                table: "Inspections",
                column: "InspecitonTypeId",
                principalTable: "InspecitonTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
