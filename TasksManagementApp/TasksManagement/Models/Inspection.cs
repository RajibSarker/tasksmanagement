﻿namespace TasksManagement.Models
{
    public class Inspection
    {
        public long Id { get; set; }
        public string Status { get; set; } = string.Empty;
        public string Comments { get; set; } = string.Empty;
        public long InspectionTypeId { get; set; }
        public InspecitonType? InspecitonType { get; set;}
    }
}
