﻿namespace TasksManagement.Models
{
    public class Status
    {
        public long Id { get; set; }
        public string StatusOptions { get; set; } = string.Empty;
    }
}
