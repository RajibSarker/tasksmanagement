﻿#nullable disable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TasksManagement.DatabaseContext;
using TasksManagement.Models;

namespace TasksManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InspecitonTypesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public InspecitonTypesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/InspecitonTypes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<InspecitonType>>> GetInspecitonTypes()
        {
            return await _context.InspecitonTypes.ToListAsync();
        }

        // GET: api/InspecitonTypes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<InspecitonType>> GetInspecitonType(long id)
        {
            var inspecitonType = await _context.InspecitonTypes.FindAsync(id);

            if (inspecitonType == null)
            {
                return NotFound();
            }

            return inspecitonType;
        }

        // PUT: api/InspecitonTypes/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutInspecitonType(long id, InspecitonType inspecitonType)
        {
            if (id != inspecitonType.Id)
            {
                return BadRequest();
            }

            _context.Entry(inspecitonType).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InspecitonTypeExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/InspecitonTypes
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<InspecitonType>> PostInspecitonType(InspecitonType inspecitonType)
        {
            _context.InspecitonTypes.Add(inspecitonType);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetInspecitonType", new { id = inspecitonType.Id }, inspecitonType);
        }

        // DELETE: api/InspecitonTypes/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteInspecitonType(long id)
        {
            var inspecitonType = await _context.InspecitonTypes.FindAsync(id);
            if (inspecitonType == null)
            {
                return NotFound();
            }

            _context.InspecitonTypes.Remove(inspecitonType);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool InspecitonTypeExists(long id)
        {
            return _context.InspecitonTypes.Any(e => e.Id == id);
        }
    }
}
