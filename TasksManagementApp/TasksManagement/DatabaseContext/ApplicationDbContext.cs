﻿using Microsoft.EntityFrameworkCore;
using TasksManagement.Models;

namespace TasksManagement.DatabaseContext
{
    public class ApplicationDbContext:DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options):base(options)
        {

        }

        #region entities
        public DbSet<InspecitonType> InspecitonTypes { get; set; }
        public DbSet<Inspection> Inspections { get; set; } 
        public DbSet<Status> Status { get; set; }
        #endregion
    }
}
